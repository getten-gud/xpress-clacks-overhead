var msg = "GNU Terry Pratchett"

module.exports = function(req, res, next){
    setHeader(res,msg);
    next()
}

// TODO: I'm not certain this is the best filter for header data.
module.exports.msg = function(headerText){
    msg = headerText.replace(/[^\x20-\x7E]+/g, '')
    return this;
}

function setHeader(res, msg){
    res.setHeader('X-Clacks-Overhead', msg );
}

