## xpress-clacks-overhead
This is free and unencumbered software released into the public domain.

[X-Clacks-Overhead](https://xclacksoverhead.org/home/about) for Express.

### Usage:

Operating it is almost too easy, the module returns the middleware method that you would use `app.use()` on, and it has another method `msg()` that sets the message and returns the module.

#### Bare Bones Express Example:

```javascript
const express = require('express')
const app = express();

const xpressClacks = require('xpress-clacks-overhead')

app.use(xpressClacks())

```

This will send the traditional `X-Clacks-Overhead:GNU Terry Pratchett` header along with your page responses.

#### Changing the message:
Feeling devious? After all, X-Clacks-Overhead in and of itself is a tribute to the master, we have some space to play with and Express(haha pun) ourselves with.

Simple change to the code above, call the `.msg` function on your require.

```javascript
const xpressClacks = require('xpress-clacks-overhead').msg('GNU Terry Pratchett SMOKING GNU')

```

It will still return the module and set the message for you. It SHOULD filter out any nasty characters for you, though I'm not entirely sure I've done that right so feel free to submit a PR if you have the knowing's of these things!